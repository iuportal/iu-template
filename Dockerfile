FROM infernial/iudrupal:v1

###### Iniciamos la instalación
WORKDIR /var/www/html

# creamos directorio scripts
RUN mkdir /var/www/html/scripts
RUN mkdir /var/www/html/backups

# Copiamos todo el directorio drupal. En el dockerignore están ignoradas las carpetas que no tiene sentido copia porque instala composer.
COPY ./drupal /var/www/html
# Copiamos los ficheros de configuración
COPY ./deploy/all/opcache.ini $PHP_INI_DIR/conf.d/
COPY ./deploy/all/docker-memory-limit.ini $PHP_INI_DIR/conf.d/
  
# Make sure file ownership is correct on the document root.
RUN chown -R www-data:www-data /var/www/html/configuration
RUN chown -R www-data:www-data /var/www/html/translations
RUN chmod +x /var/www/html/scripts/initialize.sh

# Comandos de despliegues de código y configuración
# install packages. Este comando revisa el fichero composer.json para instalar los módulos y versiones de core especificadas en el fichero
RUN composer install

#Crear volumen para files.
# Define default volues for files
VOLUME ["/var/www/html/files/"]

# Definimos un entrypoint para todas aquellas configuraciones para las que necesitamos tener la base de datos arrancada
ENTRYPOINT ["/bin/bash", "/var/www/html/scripts/initialize.sh"]
