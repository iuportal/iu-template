**Problemas**

1. Resuelto. El contenedor php se reinicia cada minuto. Solucionado. Al terminar el script del entrypoint el contenedor salía con un 0 porque ya había terminado la tarea. He añadido un "apache2ctl -D FOREGROUND" al final del script del entrypoint (TODO: no entiendo por qué la distribución no arrancaba algún proceso que mantuviera vivo el contenedor)
2. Resuelto. No encuentro manera de hacer que funcione el nginx con el php salvo montando un volumen compartido por ambos contenedores. En principio, puedo declarar un volumen ./drupal:/var/www/html pero entonces no puedo hacer el composer install en tiempo de build del contenedor sino que tengo que incluirlo en el script que se ejecuta tras el arranque del contenedor. La otra opción sería tener un nginx en el mismo contenedor que el php y montar el nginx como proxy inverso. Se ha montado un contenedor con php-drupal y apache.

**Distribución de portal**

El objetivo es mantener algo similar a una distribución de drupal con un portal adaptado a las necesidades de IU pero que permita luego la posterior evolución independiente de cada portal. Es decir, partimos de una plantilla común que luego personalizamos para cada organización.

El objetivo es instalar siguendo las instrucciones del apartado correspondiente una instancia de portal base de drupal y, a partir de ese momento, separar el desarrollo en un proyecto propio en el que todo se actualice únicamente subiendo a git el fichero composer.json, los módulos y themes custom y los json de configuración del directorio sync. El resto de ficheros ya no se mantienen en git sino que en cada actualización los gestiona composer.

**Arquitectura**

Se define un docker-compose.yml común a desarrollo y producción con un contenedor de php y apache ya que en producción la base de datos no estará dockerizada. Se define un yml adicional para cada entorno. Aún no está definido el de producción pero el de desarrollo incluye dos contenedores: una base de datos y sobreescribe algún comportamiento del contenedor drupal. Por tanto, el entorno de desarrollo se arranca y se para con los siguientes comandos:
```
docker-compose -f docker-compose.yml -f dev-docker-compose.yml up -d
docker-compose -f docker-compose.yml -f dev-docker-compose.yml down
```
Antes de definir qué parte del despliegue se debe incluir en el build del contenedor y qué parte se debe realizar con un entrypoint una vez haya arrancado, repasamos los pasos de un despliegue drupal:
- Se actualizan los ficheros de módulos y themes custom, además del composer.json y el composer.lock junto con los yml del directorio de configuración. Consiste una simple copia dentro de un volumen del contenedor de php del código custom que haya cambiado en el proyecto. Es posible que alguno de estos módulos esten en el propio repositorio git o en otros repositorios (por ejemplo, el theme podría ser un submódulo apuntando a su repositorio). Esta tarea debe hacerse en el build.
- _composer install_. Este comando lee la información de composer.lock, es decir los módulos y versiones que deberían estar instalados y se descarga aquellos módulos, themes o core que no estén instalados o estén desactualizados. Esta tarea debe hacerse en el build.
- _drush cim -y_. Este comando lee los ficheros yml que hay en el directorio de configuración y, si es necesario, actualiza la base de datos. Esta tarea no puede ejecutarse en el build porque requiere que la base de datos esté arrancada e inicializada.
- _drush updb -y_. Este comando examina los módulos y, si tienen algún update pendiente, los ejecuta contra la base de datos. Esta tarea no puede ejecutarse en el build porque requiere que la base de datos esté arrancada e inicializada.
- _drush cr_. Este comando limpia la caché, es decir, borra algunos directorios donde se están cacheando css y js y, sobre todo, borra las tablas de caché de la base de datos. Esta tarea no puede ejecutarse en el build porque requiere que la base de datos esté arrancada e inicializada.

La estructura de directorios es:

-----.env (fichero con las variables de entorno que se copia a partir de env.sample)

-----env.sample (fichero de ejemplo de variables de entorno)

-----docker-compose.yml (fichero de configuración del contenedor drupal común a los dos entornos)

-----dev-docker-compose.yml (fichero con la configuración de contenedores apropiada para desarrollo)

-----prod-docker-compose.yml (fichero con la configuración de contenedores apropiada para producción)

----->drupal (carpeta con el código custom del portal)

----->deploy

---------->backup (directorio con los ficheros de base de datos y files para la carga inicial)

---------->all (ficheros comunes para los dos entornos)

---------------initialize.sh (script entrypoint para la instalación dependiente de la base de datos)

---------------Dockerfile (fichero para construir el contenedor drupal)

---------------settings.php (fichero específico del portal generado a partir de settings.php.sample)

---------------settings.php.sample (ejemplo de configuración del portal)

---------->dev (ficheros de configuración necesaria para el entorno de desarrollo. Sólo hay una configuración de nginx que en estos momentos no se usa)


La estructura de contenedores común será:
- Un contenedor drupal (sobre una base de apache php buster) que se construye a partir de .deploy/all/Dockerfile con:
  - Un mapeo de puertos
  - Un montaje del directorio local files con /var/www/html/files que es el directorio public: de drupal.

Para la configuración de desarrollo se añaden las siguientes configuraciones:
- Se sobrescribe la configuración del contenedor drupal para añadir:
  - Una dependencia con el contenedor de mysql que sólo tiene sentido en desarrollo porque en producción la DB no está dockerizada.
  - Se habilita el contenedor drupal para que esté disponible tanto po la red externa como por la interna
  - TODO: Revisar la configuración de memoria de este contenedor que probablemente no es necesaria.
- Un contenedor de base de datos
  - Se le pasa el comando mysqld --default-authentication-plugin=mysql_native_password --skip-mysqlx porque, con la configuración por defecto de esta versión de mysql, drupal no se autenticaba.
  - Se define una secuencia para detectar si la base de datos está respondiendo
  - Se habilita el contenedor de base de datos para que sólo esté disponible por la red interna.
  - Se ha comentado un entrypoint en el que se cargaba la base de datos al arrancar el contenedor. Esto estaba bien para desarrollo pero para producción era necesario que se cargara la base de datos no cada vez que se arrancara el contenedor sino sólo en la primera instalación del portal, por lo que la carga de base de daos se ha movido a un script de un entrypoint del contenedor drupal.
  - TODO: Se está utilizando una base mysql. Hay que cambiar a una base mariadb

TODO: Dónde se rompe el canal SSL? El certificado se instala en el apache del contenedor drupal o en un servidor web frontal que haga de proxy inverso del sistema, de todos los portales? Se ha dejado un volumen certbot por si es necesario instalar el certificado en el apache drupal, ya que tendría que ser un almacenamiento persistente.

**Instrucciones de instalación**

1. Clonamos el proyecto de template con el nombre de nuestro portal:
```
git clone git@gitlab.com:iuportal/iu-template.git iuasamblea
cd iuasamblea
```
2. Desconectamos este proyecto del proyecto de plantilla eliminado ficheros y directorios .git
```
rm -rf .git
```
3. Verificar que los ficheros .gitignore y ./drupal/.gitignore tienen la configuración correcta, de modo que queden excluidos del control de versiones todos los directorios y ficheros de producto (vendor) y contribuidos y sólo mantengamos en control de versiones los siguientes elementos:
- Los módulos y temas custom que implementemos nosotros
- El listado de módulos contribuidos con versiones que estamos usando que se encuentran en el fichero composer.json y composer.lock
- La configuración de la base de datos con nuestras configuraciones convenientemente exportada en ficheros yml en el directorio de configuration

4. Modificar el fichero README.md con las características de nuestro proyecto.

5. Crear fichero para las variables de entorno. Se puede renombrar el fichero env.sample como .env
```
mv env.sample .env
```
Editar el fichero .env y darle valor a las propiedades de configuración con los valores de nuestro portal:

```
PROJECT_NAME=[prefijo del proyecto]
PROJECT_BASE_URL=[URL del portal]

DB_NAME=[Nombre de la base de datos]
DB_USER=[Usuario de la base de datos]
DB_PASSWORD=[Passwor de la base de datos]
DB_ROOT_PASSWORD=[Password de root de la base de datos]
DB_HOST=mysql
DB_DRIVER=mysql
```

Es importante asignar a la propiedad PROJECT_NAME un prefijo correcto para nuestro proyecto y diferente de otros con los que estamos trabajando. Este prefijo se utilizará para nombrar a los contenedores que se arranquen y distinguirlos de otros que tengamos arrancados en nuestra máquina.

Por ejemplo, para el entorno del desarrollo podríamos dejar los siguientes valores:

```
PROJECT_NAME=iufuen
PROJECT_BASE_URL=iufuen.docker.localhost

DB_NAME=drupal
DB_USER=drupal
DB_PASSWORD=drupal
DB_ROOT_PASSWORD=drupal
DB_HOST=mysql
DB_DRIVER=mysql
```

> Hay que tener en cuenta que este fichero habrá que editarlo en el entorno de producción con los valores reales.


6. Renombramos o copiamos el fichero deploy/all/settings.local.php.sample como settings.local.php y modificamos el settings para añadir el dominio y los parámetros de base de datos de nuestro proyecto de portal. Dejaremos los mismos que hayamos puesto en el fichero de .env
> Hay que tener en cuenta que este fichero habrá que editarlo en el entorno de producción con los valores reales.

TODO: Hay un intento que no me ha funcionado todavía de tomar los valores de la base de datos del settings.local desde el fichero de .env

7. Comprobamos que el proyecto está funcionando y arranca correctamente con las siguientes instrucciones:
- Construimos el contenedor drupal:
```
docker-compose build
```
Arrancamos la configuración de contenedores del entorno de desarrollo:
```
docker-compose -f docker-compose.yml -f dev-docker-compose.yml up -d
```

> En esta configuración el apache se expone en el puerto 8087. Si se desea ocupar otro puesto se puede editar el fichero docker-compose.yml y modificar el mapeo de puertos (el puerto interno se debe dejar siempre en el 80 o habrá que cambiar la configuración del 000-default de sites-enabled)

Monitorizamos el arranque correcto con:
```
docker logs [nombre contenedor]
```

Podemos ver los contenedores arrancados con docker ps. En nuestro caso, se muestran dos contenedores:

```
CONTAINER ID    IMAGE          COMMAND                  CREATED         STATUS                  PORTS                  NAMES
587f581cdee7    iufuen_drupal  "/bin/bash /var/www/…"   27 minutes ago  Up 27 minutes           0.0.0.0:8085->80/tcp   iufuen_php
f9f4bc28905b    mysql:8.0      "docker-entrypoint.s…"   27 minutes ago  Up 27 minutes (healthy) 3306/tcp, 33060/tcp    iufuen_db
```

Podemos entrar en la consola del contenedor de drupal ejecutando:

```
docker exec -it -w /var/www/html [prefijo de proyecto]_php bash
```

Cuando haya finalizado el arranque e inicialización de los contenedores, se puede navegar a:
http://iufuen.docker.localhost:8087/

> Es probable que haya que dar de alta en el /etc/hosts el dominio que estamos usando. Por ejemplo: 127.0.0.1       iufuen.docker.localhost
 
A continuación, deberíamos autenticarnos en el portal y revisar que todo es correcto, que se pueden crear contenidos, subir media, que no hay errores en el informe del sistema, etc.

> Si no sabemos la password del usuario admin se puede utilizar `drush uli` para generar un enlace de login automático de administrador de  un solo uso y cambiar la password.




8. Creamos un proyecto en gitlab. Creamos un nuevo proyecto git y subimos el código y exportamos la configuración con las siguientes instrucciones:

```
git init
git remote add origin git@gitlab.com:[grupo]/[iuasamblea].git
git add .
git commit -m "Initial commit"
git push -u origin master
```

> Si es necesario, podemos eliminar el remote en local: git remote rm origin

> Si ya hemos creado un readme tendremos que hacer un pull mezcalndo las dos historias: `git pull origin master --allow-unrelated-histories`


Con esto la plantilla genérica de web de IU ha quedado subida con las particularidades de un portal de asamblea concreto y a partir de este momento hay que comenzar el desarrollo específico de este portal. Se puede hacer el despliegue en producción para comprobar que funciona correctamente.

9. Despliegue en producción

TODO

**Subir los cambios al nuevo portal**

1. Salvar los cambios de site config, nuevos módulos necesarios, y modificaciones de estilo

Antes de nada debemos crear una rama de dev si no estaba ya creada. Con esto podremos ir salvando todos los cambios en el repositorio remoto sin comitear en la rama de master. Cuando el desarrollo este testado en nuestro local haremos un merge de dev a master (lo que implicará un depliegue automático del contenedor con la nueva versión en producción).

Los pasos son los siguientes:
- Modificamos desde el interface de drupal lo que necesitemos. Si necesitamos editar la css lo hacemos como habitualmente.
- Una vez finalizados los cambios en local, tenemos una serie de ficheros modificados en el filesystem pero los cambios realizados durante el site building están sólo en base de datos. Ahora necesitamos exportarlos a disco. Para eso, entramos en la consola y ejecutamos el comando de drush configuration-export:

```
docker exec -it -w /var/www/html [prefijo de proyecto]_php bash
drush cex
```
Tambien podríamos ejecutar el comando desde fuera del contenedor:

```
docker-compose -f docker-compose.yml -f dev-docker-compose.yml run [prefijo de proyecto]_php .drush --root=/var/www/html cex
```

Este comando generará o sobrescribirá los ficheros yml que se encuentran en el directorio de configuration. Una vez finalizada la operación, salimos de la consola del contenedor y revisamos los ficheros modificados:

```
git status -s
```

Es importante asegurarse en este punto que todos los ficheros que git marca como modificados, nuevos o borrados se corresponden con nuestros cambios y los entendemos.

A continuación creamos una rama de dev clonado la rama de master, añadimos los cambios, comiteamos y hacemos el push al servidor remoto:

```
git checkout -b dev
git add .
git commit -m "Creamos la rama de desarrollo"
git push --set-upstream origin dev
```

Puesto que en este caso se trata de una rama permanente, no hacemos el merge en local contra master (es posible que ni siquiera tengamos permisos para hacer push en master) ni borramos la rama al finalizar el merge. Podemos realizar el merge entre las dos ramas desde el interface de gitlab.


2. Trabjando con ramas.

Ahora estamos situados en la rama de dev. Podemos asegurarnos con un git branch

```
git branch
* dev
  master
```
Todos los cambios que hagamos se podrían comitear directamente a dev pero es una buena práctica subir cambios completos de funcionalidades haciendo una rama de funcionalidad o feature branch. Los comando son los siguientes:

```
git checkout -b fb-ldap-integration-001
git add [Los ficheros que compongan esta feature]
git commit -m "[La descripción del cambio]"

# Subimos la rama de feature al servidor remoto
git push --set-upstream origin dev

# Hacemos el merge entre la rama de feature y la rama de dev
git checkout dev
git merge fb-ldap-integration-001

# Borramos la rama en local
git branch -d fb-ldap-integration-001
# Podemos dejarla en el servidor por razones de auditoria hasta que se mergee con master o borrarla inmediatamente
git branch -d -r origin/fb-ldap-integration-001
```


3. Despliegues automáticos.
Cuando hagamos push a la rama master se desplegará automáticamente en producción una nueva versión del contenedor

TODO

**Anexos**

**Dockerfile del contenedor drupal**

El dockerfile del contenedor drupal es el siguiente:
```
FROM drupal:9.0.7-apache-buster

# Install packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    curl \
    wget \
    libpng-dev \
    zlib1g-dev \
    git \
    zip \
    libzip-dev \
    unzip 
    
# Instalamos imagemagick. Compilo porque no encuentro paquete para buster
RUN cd /tmp && wget https://www.imagemagick.org/download/ImageMagick.tar.gz && \
    tar xvzf ImageMagick.tar.gz && cd ImageMagick-7.0.10-38/ && \
    ./configure && \
    make && \
    make install && \
    ldconfig /usr/local/lib 
    
# Revisar esto para usar en lugar de este paquete el de buster mariadb-client
RUN apt-get install --no-install-recommends -y default-mysql-client

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    ln -s /root/.composer/vendor/bin/drush /usr/local/bin/drush

# Install Drush
RUN composer global require drush/drush && \
    composer global update

# Clean repository
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# install mysql
RUN docker-php-ext-install mysqli 
RUN docker-php-ext-enable mysqli

###### Iniciamos la instalación
WORKDIR /var/www/html

# creamos directorio scripts
RUN mkdir /var/www/html/scripts
RUN mkdir /var/www/html/backups
RUN mkdir /var/www/html/translations

# copy files
COPY drupal/composer.json /var/www/html/
COPY drupal/composer.lock /var/www/html/
COPY drupal/configuration /var/www/html/configuration
COPY drupal/modules/custom /var/www/html/modules/custom
COPY drupal/themes/custom /var/www/html/themes/custom
COPY drupal/libraries /var/www/html/libraries
COPY deploy/all/settings.php /var/www/html/sites/default/
COPY deploy/all/initialize.sh /var/www/html/scripts/
COPY deploy/backup/db_initial.sql /var/www/html/backups/
COPY deploy/backup/files.tar.gz /var/www/html/backups/

# Make sure file ownership is correct on the document root.
RUN chown -R www-data:www-data /var/www/html/configuration
RUN chown -R www-data:www-data /var/www/html/translations
RUN chmod +x /var/www/html/scripts/initialize.sh

# Comandos de despliegues de código y configuración
# install packages. Este comando revisa el fichero composer.json para instalar los módulos y versiones de core especificadas en el fichero
RUN composer install

# Definimos un entrypoint para todas aquellas configuraciones para las que necesitamos tener la base de datos arrancada
ENTRYPOINT ["/bin/bash", "/var/www/html/scripts/initialize.sh"]
```
que realiza las siguientes tareas:
- Extiende el contenedor drupal:9.0.7-apache-buster
- Instala una serie de comandos útiles
- Instala la librería de imagemagick necesitada por este portal. TODO: No se ha encontrado paquete para buster así que se ha compilado
- Instala el cliente de mysql
- Instala composer 
- Instala drush 
- Copia la estructura custom de este portal, es decir, los ficheros composer.json y composer.lock, los directorios de themes, modules, libraries, settings, ficheros para la carga inicial y el script initialize.sh que finalizará la instalación una vez esté arrancado el contenedor.
- Asigna propietario y permisos de las carpetas
- Ejecuta composer install para que dependiendo de los módulos declarados en composer.lock se descargue el código adecuado.
- Define un entrypoint con el script initialize.sh

TODO: El fichero de settings hay que dividirlo en un settings y un setting.local en el que se almacene la parte específica de este portal.
TODO: Asignar como propietario de todos los directorios al usuario root excepto files, translations. En realidad, en un entorno de producción www-data sólo tendría que escribir en los directorios anteriores, pero en un entorno de desarrollo se debe ser capaz de escribir en modules, themes, configuration y libraries. Analizar si se añade una variable de entorno para que initialize asigne unos permisos u otros o existe una alternativa más fina jugando con volúmenes.

El script initialize.sh realiza algunas tareas de inicialización del entorno y todas aquellas tareas para las que se requiere que la base de datos esté arrancada:

```
#!/bin/bash
echo Esperamos a que la base de datos esté preparada
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"
while ! mysqladmin ping -h mysql --silent; do
    sleep 1
done
# Use a table that should exist in your database. Como esto es sólo para la carga inicial de la DB y el resto de cambios se harán importando configuración preguntamos simplementr por la tabla node
if ! drush sql-query 'SELECT * FROM node;' 2>/dev/null; then
  echo Iniciamos carga de base de datos
  cat /var/www/html/backups/db_initial.sql | drush sql-cli
fi
# Chequeamos si el directorio files ha sido inicializado. La primera vez que arranca el contenedor hay que descomprimir el 
FILE=/var/www/html/files/.htaccess
if [ -f "$FILE" ]; then
    echo "El directorio files ya ha sido inicializado"
else 
    echo "Es la primera vez que arranca el contenedor y es necesario desempaquetar los ficheros en public:"
    tar xvfz /var/www/html/backups/files.tar.gz -C /var/www/html/files/.
    chown -R www-data:www-data /var/www/html/files
fi

echo Iniciamos carga de la configuración
drush cim -y
echo Finalizada carga de la configuración e iniciamos actualización de base de datos
drush updb -y
echo Limpiamos caché
drush cr
echo Finalizado el despliegue a las
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"

apache2ctl -D FOREGROUND
```
- Ejecuta un bucle preguntando mysqladmin ping -h mysql para saber si la DB ya está respondiendo. 
- Lanza esta query drush sql-query 'SELECT * FROM node para averiguar si la base de datos ya está inicializada. Pregunta por la tabla node que sólo existirá si la base de datos tiene un esquema drupal. En producción la creación de la base de datos sólo se producirá la primera vez ya que se trata de un proceso persistente con dockerizado. En desarrollo, la carga de la base de datos se producirá cada vez que se rearranquen los contenedores. TODO: Buscar una select más específica de este profile.
- Si no se ha cargado la base de datos, inicia la carga con cat /var/www/html/backups/db_initial.sql | drush sql-cli
- Chequea la existencia de un fichero .htaccess en el directorio files. El directorio files es un volumen persistente (link mount) que deberá existir en el filesystem tanto de la máquina de desarrollo como en producción. Se asume que si no tiene el fichero .htaccess es que nunca se ha inicializado y, en ese caso, se hace un untar de los ficheros de este template de portal. Esta acción sólo se realizará una vez, tanto en el entorno de desarrollo como en producción.
- Importa la configuración de los .yml en la base de datos con drush cim. Esta acción se ejecuta siempre. Si no se ha modificado ningún fichero de la carpeta de configuration no hace nada.
- Aplica los updates de los módulos contrib y custom. Esta acción se ejecuta siempre. Si no hay nuevas funciones update que ejecutar no hace nada.
- Limpia la caché con drush cr
- Arranca el proceso del servidor web Apache.

**Política de backups**

Los únicos elementos de las que habrá que hacer backup son la base de datos y el directorio files. El resto se encuentra en git o en los repositorios que corresponda.

**Comandos para generar los ficheros de inicialización**

Para generar el files.tar.gz:
```
cd files
tar cvfz ../backup/files.tar.gz .
```

Todos
- El cron no está configurado. Habría que instalar algún módulo de gestión del cron tipo elysia
- No he configurado Traefik en este desarrollo. Lo necesitamos en producción?
- La template debería ser un submódulo


Apuntes

- Usar config split y config ignore https://www.drupal.org/project/config_ignore/releases/8.x-3.x-dev
- 12 factor apps https://12factor.net/
- “12-factor“ize drupal https://www.shapeblock.com/using-drupal-and-docker-in-production/
- Se puede tunear el php con los siguientes parámetros (estaban bien para D7, no sé si para D9)

```
# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini
```

- Revisar este Dockerfile para optimizar el mío: https://github.com/Lullabot/drupal8ci/blob/master/Dockerfile