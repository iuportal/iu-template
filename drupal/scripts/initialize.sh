#!/bin/bash
echo Esperamos a que la base de datos esté preparada
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"
# Hay que pasarle el nombre de la base de datos por variables de entorno
while ! mysqladmin ping -h $DB_HOST --silent; do
    sleep 1
done
# Use a table that should exist in your database. Como esto es sólo para la carga inicial de la DB y el resto de cambios se harán importando configuración preguntamos simplementr por la tabla node. Revisar query show tables
if ! drush sql-query 'SELECT * FROM node;' 2>/dev/null; then
  echo Iniciamos carga de base de datos
  cat /var/www/html/backups/db_initial.sql | drush sql-cli
fi
# Chequeamos si el directorio files ha sido inicializado. La primera vez que arranca el contenedor hay que descomprimir el
FILE=/var/www/html/files/.htaccess
if [ -f "$FILE" ]; then
    echo "El directorio files ya ha sido inicializado"
else
    echo "Es la primera vez que arranca el contenedor y es necesario desempaquetar los ficheros en public:"
    tar xvfz /var/www/html/backups/files.tar.gz -C /var/www/html/files/.
    chown -R www-data:www-data /var/www/html/files
fi

echo Si estamos en el entorno de desarrollo hay que hacer el composer install
composer install
echo Finalizada carga de la configuración e iniciamos actualización de base de datos
drush updb -y
echo Iniciamos carga de la configuración
COMPOSER_MEMORY_LIMIT=-1 drush cim -y
echo Limpiamos caché
drush cr
echo Finalizado el despliegue a las
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "$dt"

apache2ctl -D FOREGROUND
