<?php

/**
 * @file
 * CI/LOCAL Settings.
 * @NOTE Uncomment local/ci config according to desired environment.
 */

/**
 * DATABASE.
 */
// Database connection. Comentamos esto hasta que consigamos que lleguen las variables de entorno
/*
$databases = [
  'default' =>
  [
    'default' =>
    [
      'database' => getenv('DB_NAME'),
      'username' => ('DB_USER'),
      'password' => getenv('DB_PASSWORD'),
      'host' => getenv('DB_HOST'),
      'port' => getenv('DB_PORT'),
      'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
      'driver' => 'mysql',
    ],
  ],
];
*/
/*
$databases = [
  'default' =>
  [
    'default' =>
    [
      'database' => 'drupal',
      'username' => 'drupal',
      'password' => 'drupal',
      'host' => 'mysql',
      'port' => '3306',
      'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
      'driver' => 'mysql',
    ],
  ],
];
*/
/**
 * CI: ENVIRONMENT INDICATOR.
 */
//$conf['environment_indicator_overwritten_name'] = 'LOCAL';
//$conf['environment_indicator_overwritten_color'] = 'GREEN';
//$conf['environment_indicator_overwrite'] = TRUE;

/**
 * LOCAL: STAGE FILE PROXY.
 */
/* $conf['stage_file_proxy_origin'] = 'http://www.ejemplo.com'; */

/**
 * REROUTE EMAIL.
 */
//$conf['reroute_email_address'] = 'debug@example.com';
//$conf['reroute_email_enable'] = TRUE;

/**
 * DEBUG.
 */
//error_reporting(-1);
//$conf['error_level'] = 2;
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);


/**
 * CACHE AND TEST OPTIMIZATION FOR DEV/CI ENVIRONMENTS.
 */
// Test optimization, do not use in prod environments.
/*$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['lock_inc'] = 'sites/all/modules/contrib/memcache/memcache-lock.inc';
$conf['memcache_stampede_protection'] = FALSE;
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
$conf['cache_class_watchdog'] = 'DrupalDatabaseCache';
$conf['page_cache_without_database'] = TRUE;
$conf['page_cache_invoke_hooks'] = FALSE;
$conf['memcache_servers'] = ['memcached:11211' => 'default'];
$conf['memcache_log_data_pieces'] = 10;
*/

// En el entorno de producción todo esto debería estar descomentado
//$conf['cache'] = TRUE;
//$conf['page_compression'] = TRUE;
//$conf['preprocess_css'] = TRUE;
//$conf['preprocess_js'] = TRUE;
//$conf['theme_debug'] = FALSE;
